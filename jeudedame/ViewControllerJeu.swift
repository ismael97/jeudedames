//
//  ViewControllerJeu.swift
//  jeudedame
//
//  Created by Said Ismael on 07/04/2021.
//

import UIKit

class ViewControllerJeu: UIViewController {
    let partie=Partie(
        user1:Utilisateur(nomUtilisateur: "j1",direction: .sud),
        user2:Utilisateur(nomUtilisateur: "j2",direction: .nord),
        damier:Damier(nbligne: 10, nbColonnes: 10)
    )
    
    
    @IBOutlet weak var boardview: BoardView!
    // function which is triggered when handleTap is called
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        //      Récupération de la taille d'écran et de la position du toucher
        let tailleEcran=boardview.bounds.width
        let longueurCarre=tailleEcran/10
        
        //        On récupère la case qui a était cliqué
        let colonne:Int=Int((sender.location(in: boardview).x)/longueurCarre)
        let ligne:Int=Int((sender.location(in: boardview).y)/longueurCarre)
        
        switch partie.etat  {
        case .selectionPiece:
            print("etat: selection piece")
            if(partie.contientUnePieceDeplacable(position: Position(ligne: ligne, colonne: colonne))){
                
                partie.selectionPiece(ligne: ligne, colonne: colonne)
                
                majVue()
                
            }
            
        case .deplacementPiece:
            print("etat deplacement piece selectionnée")
//            si la position cliqué est une case accessible par la piece selectionné, déplacez la piece vers cette position
        let position = Position(ligne: ligne, colonne: colonne)
            print("ligne",ligne)
            print("colonne",colonne)
            if partie.estUneCaseAccessible(piece: partie.joueurCourant.selection!, position: position) {
                print("deplacement de la piece")
                partie.deplacerPiece(position: Position(ligne: ligne, colonne: colonne))
                majVue()
            }
        default:
            break
        }
        
        
    }
    
    @IBOutlet weak var myview: BoardView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //innitialisation de boardview
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        boardview.addGestureRecognizer(tap)
        //ajout d'une couleur par défault pour les deux joueurs dans la partie
        partie.joueurCourant.setColor(couleur: UIColor(red:0, green:0, blue:0, alpha:0.8).cgColor)
        partie.joueurSuivant.setColor(couleur: UIColor(red:1, green: 1, blue:1, alpha:0.8).cgColor)
        
        
        //        On passe le damier et la partie a la vue
        //        partie.initPieceDeplacable()
        boardview.partie=partie
        
        
        
    }
    
    func majVue(){
        boardview.maj()
        
    }
    
    
    
}
