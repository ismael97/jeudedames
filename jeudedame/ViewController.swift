//
//  ViewController.swift
//  jeudedame
//
//  Created by Said Ismael on 05/04/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func lancerPartie(_ sender: Any) {
//        Débuggage et vérification du fonctionnement
//        ici devra être fait l'innitialisation de la partie
//        1: placer les pieces du joueur1
//        2:Placer les pieces du joueur2
        print("Lancer partie Trigger")
        
        performSegue(withIdentifier:"versPartie", sender: self)

        
    }
    
}

