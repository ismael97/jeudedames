//
//  File.swift
//  jeudedame
//
//  Created by Said Ismael on 06/04/2021.
//

import Foundation

class Position{
    var l:Int
    var c:Int
    var piece:Piece?
    
    init(ligne:Int,colonne:Int,piece:Piece) {
        l=ligne
        c=colonne
        self.piece=piece
    }
    init(ligne:Int,colonne:Int) {
        l=ligne
        c=colonne
    }
    
    //    Fonction estOccupe
    //    e:
    //    s: Vrai si la position est occupé par une piece, faux sinon
    func estOccupe() ->Bool {
        return !(piece==nil)

    }
    
    //    Fonction getLigne
    //    e:
    //    s:
    
    func getLigne() ->Int{
        return self.l
    }
    
    //    Fonction getCol
    //    e:
    //    s:
    
    func getColonne() ->Int{
        return self.c
    }
    
    //    Fonction setPiece
    //    e:
    //    s:
    func setPiece(piece:Piece){
        self.piece=piece
    }
    
    //    Fonction deletePiece
    //    e:
    //    s:
    //effet de bord: Supprimer la pièce a cette position
    func deletetPiece(){
        self.piece=nil
    }
    
    
    
//    Fonctions de débuggage
    func toString() -> String {
        let res = "["+String(self.l)+"]"+"["+String(self.c)+"]"
        return res
    }
}
