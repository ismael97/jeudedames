//
//  Damier.swift
//  jeudedame
//
//  Created by Said Ismael on 05/04/2021.
//

import Foundation
class Damier{
    var nbligne:Int
    var nbColonnes:Int
    var Board=[[Position]]()
    
    
    
    init(nbligne:Int,nbColonnes:Int) {
        self.nbColonnes=nbColonnes
        self.nbligne=nbligne
        
        //Innitialisation des positions du damiers
        Board=Array(repeating: Array(repeating: Position(ligne: 0, colonne: 0), count: nbligne), count: nbColonnes)
        
        for i in 0..<nbligne{
            for j in 0..<nbColonnes{
                self.Board[i][j]=Position(ligne: i, colonne: j)
            }
            
        }
        
    }
    
    func recuperPiece(ligne:Int,colonne:Int) -> Piece {
        return Board[ligne][colonne].piece!
    }
    
    init(){
        nbligne=0
        nbColonnes=0
        Board=[[Position]]()
    }
    
    //e: Une Piece piece, une position Position
    //s: rien
    //effet de bord: Rajoute piece sur la Position postion
    //Utile pour innitialiser
    
    func ajouterPieceSurDamier(p:Piece,pos:Position){
        let l = pos.getLigne()
        let c = pos.getColonne()
        if !Board[l][c].estOccupe(){
//            Si la position est libre
            Board[l][c].setPiece(piece: p)
            
        }
    }
    
    func estLibre(ligne:Int,colonne:Int) -> Bool {
        return !Board[ligne][colonne].estOccupe()
    }
    
    func ajouterPieceSurDamier(p:Piece){
        let l = p.position.getLigne()
        let c = p.position.getColonne()
        if !Board[l][c].estOccupe(){
//            Si la position est libre
            Board[l][c].setPiece(piece: p)
            
        }
    }
    
    
//    e: une Piece piece
//    s:
//    Postcondition:

    func supprimerPieceDuDamier(piece:Piece,position:Position){
        self.Board[position.getLigne()][position.getColonne()].deletetPiece()
        
        
    }
    
    //    e: une Piece piece, une position Position
    //    s:
    //    Postcondition:
    func deplacementSansPrise(piece:Piece,position:Position){
        
    }
    
    func deplacementAvecPrise(){
        
    }

    
    
    //e:
    //s:La réprésentatioin textuelle du damier
    
    func damierToString() -> String {
        var res=""
        
        for i in 0..<nbligne{
            for j in 0..<nbColonnes{
                res+=Board[i][j].toString()

            }
            res+="\n"
            
        }
        return res
    }
    
    func estOccupe(ligne:Int,colonne:Int) ->Bool {
        
        
        return Board[ligne][colonne].estOccupe()
    }
    
    //e:
    //s:Renvoie le contenu du tableau sous forme de chaines de charactères
    //Permet de verifier le bon fonctionnement de deplacerPieces()
    func contenuDamierToString(){
        
    }
    
    func supprimerPiece(position:Position){
        Board[position.getLigne()][position.getColonne()].piece=nil
    }
    
    
}
