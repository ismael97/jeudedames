//
//  Piece.swift
//  jeudedame
//
//  Created by Said Ismael on 05/04/2021.
//

import Foundation


class Piece{
//    Chaque piece a besoin d'un état pour savoir si il est Reine ou pas
    var owner:Utilisateur
    var position:Position
    
    
    
    
    init(owner:Utilisateur,position:Position) {
        self.owner=owner
        self.position=position
        
    }
    
    
//    Chaque piece doit être capable de retourner les cases qui lui sont directement accessibles
//    On ne tient pas compte a ce niveau si la position est occupée ou pas
    func casesAccessibles()->[Position]{
        var result = [Position]()
        let x=position.getLigne()
        let y=position.getColonne()
        
//        diagonale SUD-EST
//        Vérifiez que il s'agit d'une position valide
//        Ajoutez au résultat
        
        if (x+1<10 && y+1<10) {
            result.append(Position(ligne: x+1, colonne: y+1))
        }
        
        //        Diagonal SUD-OUEST
        
        if (x+1<10 && y-1>=0) {
            result.append(Position(ligne: x+1, colonne: y-1))
        }
        
        
//        Diagonal NORD-OUEST
        
        if (x-1>=0 && y-1>=0) {
            result.append(Position(ligne: x-1, colonne: y-1))
        }
        

//        Diagonal NORD-EST
        
        if (x-1>=0 && y+1<10) {
            result.append(Position(ligne: x-1, colonne: y+1))
        }
        
        
        return result
    }
    
    
    func getPosition() -> Position {
        return position
    }
    
    
    
    
    
//    Définir une fon
}
