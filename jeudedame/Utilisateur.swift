//
//  Utilisateur.swift
//  jeudedame
//
//  Created by Said Ismael on 05/04/2021.
//
import UIKit
import Foundation
enum Direction {
    case nord
    case sud
    case nd
}
class Utilisateur{
    var nomUtilisateur:String
    var couleur:CGColor
    var pieces=[Piece]()
    var direction:Direction
    var selection:Piece?
    
//    var pieces:[Position]

    
    
//    Constructeur
    init (nomUtilisateur:String,direction:Direction)  {
        self.nomUtilisateur = nomUtilisateur
        couleur=UIColor.black.cgColor
        self.direction = direction
        selection=nil
      }
    
    
    func selectionPiece(piece:Piece){
        selection=piece
    }
    func setColor(couleur:CGColor){
        self.couleur=couleur
    }
    
    func getColor()->CGColor{
        return couleur
    }
    
    func ajouterPiece(piece:Piece){
        self.pieces.append(piece)
    }
    
    func getNbPiece() -> Int {
        return pieces.count
    }
    
    func getPieces()->[Piece]{
        return pieces
    }
    
    init(){
//        Constructeur par défaut
        nomUtilisateur=""
        couleur=UIColor.black.cgColor
        direction = .nd
    }
    
    func getIndicePiece(piece:Piece) -> Int {
        var i = 0
        
        while i<getNbPiece() && !(pieces[i].getPosition().getLigne()==piece.getPosition().getLigne()&&pieces[i].getPosition().getColonne()==piece.getPosition().getColonne()) {
            i+=1
        }
        
        return i
        
    }
    
    func majListesPieces(anciennePiece:Piece,nouvellePiece:Piece){
        let indicePiece=getIndicePiece(piece: anciennePiece)
        print(indicePiece)
        pieces.remove(at: indicePiece)
        pieces.append(nouvellePiece)
        
        
        
    }
    
//    mettre a jour la liste des ces pieces quand une pices est deplacé
    
}
