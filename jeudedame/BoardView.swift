//
//  BoardView.swift
//  jeudedame
//
//  Created by Said Ismael on 25/04/2021.
//

import UIKit

class BoardView: UIView {
    //Innitialisation
    //    var damier=Damier()
    var partie:Partie = Partie()
    
    
    //Tests
    
    override func draw(_ rect: CGRect) {
        print("maj vue")
        guard let currentContext=UIGraphicsGetCurrentContext() else {
            print("ne peut pas récupérer le contexte")
            return
        }
        let longueurCarre=bounds.width/CGFloat(10)
        //Représentation graphique du damier
        //Alternance de couleur
        var plein=false
        for i in 0..<10{
            for j in 0..<10{
                dessinerCase(user: currentContext, estPleine: plein, x: CGFloat(j)*longueurCarre, y: CGFloat(i)*longueurCarre, largeur: longueurCarre, longueur: longueurCarre)
                //              Si la case contient une piece deplacable traitement
                
                
                plein=(!plein)
                if partie.damier.Board[i][j].estOccupe() {
                    dessinerCercle(user:currentContext, ligne: CGFloat(j)*longueurCarre+longueurCarre/2, colonne: CGFloat(i)*longueurCarre+longueurCarre/2, rayon: ((longueurCarre/2))*0.8,couleur:partie.damier.Board[i][j].piece!.owner.getColor())
                    
                    
                    
                    
                    
                }
                
                if partie.etat == .selectionPiece {
                    if partie.contientUnePieceDeplacable(position:Position(ligne: i, colonne: j)) {

                        surbrillanceCaseAccesible(user: currentContext, x: CGFloat(j)*longueurCarre, y: CGFloat(i)*longueurCarre, largeur: longueurCarre, longueur: longueurCarre)
                    }
                }
                if partie.etat == .deplacementPiece {

                    
                    
                    if partie.estUneCaseAccessible(piece: partie.joueurCourant.selection!, position: Position(ligne: i, colonne: j)) {
                        surbrillanceCaseAccesible(user: currentContext, x: CGFloat(j)*longueurCarre, y: CGFloat(i)*longueurCarre, largeur: longueurCarre, longueur: longueurCarre)
                        
                    }
                    
                }
                
                
            }
            if !plein{
                plein=true
            }else{
                plein=false
            }
        }
    }
    
    //    Permet de dessiner des cercles qui vont représenter les pions
    func dessinerPion(user context:CGContext,couleur:UIColor){
        
        
    }
    
    func dessinerBoard(damier:Damier){
        setNeedsDisplay()
    }
    
    func dessinerCase(user context:CGContext,estPleine:Bool,x:CGFloat,y:CGFloat,largeur:CGFloat,longueur:CGFloat){
        //        UIColor(red: 0.3, green: 0.5, blue: 0, alpha: 0.9).setFill()
        if estPleine {
            context.setFillColor(UIColor(red:0.5, green:0.5, blue:0, alpha:0.7).cgColor)
            
        }else{
            context.setFillColor(UIColor(red:0.6, green:0.6, blue:0, alpha:0.5).cgColor)
        }
        let carre = CGRect(x:x, y:y, width: largeur, height: longueur)
        context.addRect(carre)
        context.fillPath()
        
    }
    
    func surbrillanceCaseAccesible(user context:CGContext,x:CGFloat,y:CGFloat,largeur:CGFloat,longueur:CGFloat){
        //        UIColor(red: 0.3, green: 0.5, blue: 0, alpha: 0.9).setFill()
        context.setFillColor(UIColor(red:0.4, green:0.7, blue:0.4, alpha:0.5).cgColor)
        let carre = CGRect(x:x, y:y, width: largeur, height: longueur)
        context.addRect(carre)
        context.fillPath()
        
    }
    
    func dessinerCercle(user context:CGContext,ligne:CGFloat,colonne:CGFloat,rayon:CGFloat,couleur:CGColor){
        let centre = CGPoint(x:ligne, y:colonne)
        context.setFillColor(couleur)
        context.addArc(center: centre, radius: rayon, startAngle: 0,endAngle: CGFloat(2*Double.pi), clockwise: true)
        context.fillPath()
        
        
        
    }
    
    //Evenement déclenché lorsque un utilisateur touche la view représentant le damier
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in:self)
            //            identifier la case selectionnée
            let longueurCarre=bounds.width/CGFloat(10)
            let colonne:Int=Int(position.x/longueurCarre)
            let ligne:Int=Int(position.y/longueurCarre)
            
            
        }
        
        
        
        
        //1 Récupérer la case clqué si elle peut se déplacer
        //
        //2activer la case cliqué
        //Redessiner la grille
        
    }
    
    func maj(){
        setNeedsDisplay()
    }
    
    
}
