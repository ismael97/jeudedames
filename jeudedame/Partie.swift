//
//  Partie.swift
//  jeudedame
//
//  Created by Said Ismael on 06/04/2021.
//

enum ETAT {
    case selectionPiece
    case deplacementPiece
    case bougerPiece
    case nd
}
import Foundation
class Partie {
    var joueurCourant:Utilisateur
    var joueurSuivant:Utilisateur
    var damier:Damier
    var pieceDeplacables:[Piece]
    var etat:ETAT
    
    init(user1:Utilisateur,user2:Utilisateur,damier:Damier) {
        self.joueurCourant=user1
        self.joueurSuivant=user2
        self.damier=damier
        self.pieceDeplacables=[Piece]()
        self.etat = .selectionPiece
        departJoueur1()
        departJoueur2()
        initPieceDeplacable()
        
    }
    
    init(){
        joueurCourant=Utilisateur()
        joueurSuivant=Utilisateur()
        damier=Damier()
        self.pieceDeplacables=[Piece]()
        self.etat = .nd
        
    }
    
    //    Retourne le joueur courant
    func getJoueurCourant() -> Utilisateur {
        return joueurCourant
    }
    
    func contientUnePieceDeplacable(position:Position)->Bool{
        for p in pieceDeplacables{
            
            if position.getLigne()==p.getPosition().getLigne() && position.getColonne()==p.getPosition().getColonne() {
                return true
            }
        }
        return false
        //        var i = 0
        //        while i<pieceDeplacables.count {
        //            <#code#>
        //        }
        
    }
    
    func selectionPiece(ligne:Int,colonne:Int){
        let piece:Piece=damier.recuperPiece(ligne: ligne, colonne: colonne)
        joueurCourant.selectionPiece(piece: piece)
        self.etat = .deplacementPiece
    }
    
    
    func initPieceDeplacable(){
        self.pieceDeplacables=pieceDeplacable()
    }
    
    
    //    place les pions sur leurs positions de déppart
    func departJoueur1(){
        for i in 0..<4{
            for j in 0..<damier.nbColonnes{
                if i%2==0 {
                    if !(j%2==0) {
                        //                        Ajouter a la liste des pieces du joueur1
                        let piece:Piece=Piece(owner: joueurCourant, position: Position(ligne: i, colonne: j))
                        joueurCourant.ajouterPiece(piece: piece)
                        damier.ajouterPieceSurDamier(p:piece)
                    }
                }else{
                    if (j%2==0) {
                        let piece:Piece=Piece(owner: joueurCourant, position: Position(ligne: i, colonne: j))
                        joueurCourant.ajouterPiece(piece: piece)
                        damier.ajouterPieceSurDamier(p:piece)
                        damier.ajouterPieceSurDamier(p:Piece(owner: joueurCourant, position: Position(ligne: i, colonne: j)))
                    }
                    
                }
            }
            
        }
    }
    
    func departJoueur2(){
        for i in 6..<damier.nbColonnes{
            for j in 0..<damier.nbColonnes{
                if i%2==0 {
                    if !(j%2==0) {
                        //                        Ajouter a la liste des pieces du joueurs2
                        let piece:Piece=Piece(owner: joueurSuivant, position: Position(ligne: i, colonne: j))
                        joueurSuivant.ajouterPiece(piece: piece)
                        
                        damier.ajouterPieceSurDamier(p:Piece(owner: joueurSuivant, position: Position(ligne: i, colonne: j)))
                    }
                }else{
                    if (j%2==0) {
                        let piece:Piece=Piece(owner: joueurSuivant, position: Position(ligne: i, colonne: j))
                        joueurSuivant.ajouterPiece(piece: piece)
                        damier.ajouterPieceSurDamier(p:Piece(owner: joueurSuivant, position: Position(ligne: i, colonne: j)))
                    }
                    
                }
            }
            
        }
    }
    
    //    Retourne le prochain joueur
    func getJoueurSuivant() -> Utilisateur {
        return joueurSuivant
    }
    
    func rafflePossible() ->Bool {
        //        calculer les raffles possibles pour le joueur courant
        
        
        return false
    }
    
    
    
    //    sortie: retourne les positions accessibles pour une piece vers l'avant
    func caseAccessibles(piece:Piece)->[Position]{
        //        sera dépendant du type de pion
        var result=[Position]()
        
        switch getJoueurCourant().direction {
        case .sud:
            print("ok")
            
            for p in getJoueurCourant().getPieces(){
                let x = p.position.getLigne()
                var y = p.position.getColonne()
                if (damier.estOccupe(ligne:x+1, colonne:y+1)){
                    //                    Ajouter la position au cases accessibles
                }
            }
            
            
        default:
            break
        }
        
        return result
    }
    
    
    //    e: piece une piece et position une Position
    //    s: si position est accesible par la piece, retourne vrai. Faux sinon
    //    Dépend du type de pion
    func estUneCaseAccessible(piece:Piece,position:Position) -> Bool {
        for pos in deplacementAvant(piece: piece, direction: joueurCourant.direction) {
            
            if pos.getLigne() == position.getLigne() && pos.getColonne()==position.getColonne(){
                return true
            }
        }
        return false
    }
    
    
    
    //    Pour une piece donnée retourne la liste des positions ou la piece peut aller
    //    Dépend du joueur : vers Nord ou surd
    
    //    dépend tu type de piece
    func deplacementAvant(piece:Piece,direction:Direction)->[Position]{
        var result = [Position]()
        let caseAccessible=piece.casesAccessibles()
        
        switch direction {
        case .nord:
            for p in caseAccessible{
                if p.getLigne()<piece.position.getLigne() && damier.estLibre(ligne: p.getLigne(), colonne: p.getColonne()){
                    result.append(p)
                }
            }
        case .sud:
            
            for p in caseAccessible{
                if p.getLigne()>piece.position.getLigne() && damier.estLibre(ligne: p.getLigne(), colonne: p.getColonne()){
                    result.append(p)
                }
            }
            
        default:
            break
        }
        
        return result
    }
    
    
    
    //    retourne l'ensemble des pièce que l'on peut effectivement deplacé
    //
    
    func pieceDeplacable()->[Piece] {
        
        var result=[Piece]()
        
        let deplacementObligatoire=false
        
        if deplacementObligatoire {
            print("traitement")
        }else{
            //            si il n'y a aucun déplacement obligatoire
            for p in joueurCourant.getPieces() {
                
                let deplacementavant=deplacementAvant(piece: p, direction:joueurCourant.direction)
                
                
                for pi in deplacementavant {
                    
                    if damier.estLibre(ligne:pi.getLigne(), colonne:pi.getColonne()) {
                        result.append(p)
                        break
                    }
                }
                
                
            }
        }
        //        print(result.count)
        return result
    }
    
    
    //met a jour le joueur courant
    
    func majJoueurEnCours(){
        let tmp:Utilisateur=joueurCourant
        joueurCourant=joueurSuivant
        joueurSuivant=tmp
        initPieceDeplacable()
    }
    
    func deplacerPiece(position:Position){
        let piece = joueurCourant.selection
        
        //                1mettre a jour la liste des pieces de l'utilisateur courant
        //                2mettre a jour le damier
        let positionSelection = piece?.position
        let nouvellePiece=Piece(owner: joueurCourant, position:position)
        
        damier.supprimerPiece(position:positionSelection!)
        damier.ajouterPieceSurDamier(p: nouvellePiece, pos: position)
        self.etat = .selectionPiece
        
        joueurCourant.majListesPieces(anciennePiece:piece!,nouvellePiece:nouvellePiece)
        
        majJoueurEnCours()
        
        //        initPieceDeplacable()
        
        
    }
    
    
    
}
